import React, { Component } from 'react';
import Toolbar from '../navigation/toolbar/Toolbar';
import SideDrawer from '../navigation/side-drawer/SideDrawer';
import './Layout.css';

class Layout extends Component {

    state = {
        showSideDrawer: false,
    }

    openSideDrawerHandler = () => {
        this.setState({showSideDrawer: true});
    }

    closeSideDrawerHandler = () => {
        this.setState({showSideDrawer: false})
    }

    render() {
        return (
            <React.Fragment>
                <Toolbar showSideDrawer={this.openSideDrawerHandler}/>
                <SideDrawer
                    open={this.state.showSideDrawer}
                    closeSideDrawer={this.closeSideDrawerHandler}/>
                <div>Toolbar, SideDrawer, Backdrop</div>
                <main className="content">
                    {this.props.children}
                </main>
            </React.Fragment>
        )
    }
}

export default Layout;