import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import { Redirect } from 'react-router-dom';

class Logout extends React.Component {

    componentDidMount() {
        this.props.onLogout(this.props.token);
    }

    render () {
        return (
            <Redirect to="/" />
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: (token) => dispatch(actions.logout(token)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);