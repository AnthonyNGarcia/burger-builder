import React, { Component } from 'react';
import Button from '../../ui/button/Button';

class OrderSummary extends Component {

    // componentDidUpdate() {
    //     console.log("[OrderSummary] DidUpdate");
    // }

    render() {
        const ingredientSummary = [];

        for (let ingredient in this.props.ingredients) {
            let ingredientCount = this.props.ingredients[ingredient];
            ingredientSummary.push(
                (<li key={ingredient}>
                    <span style={{textTransform: 'capitalize'}}>{ingredient}: {ingredientCount}</span>
                </li>)
            )
        }

        return (
            <React.Fragment>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed(2)}</strong></p>
                <p>Continue to Checkout?</p>
                <Button
                buttonType="danger"
                clicked={this.props.cancelPurchase}
                >CANCEL</Button>
                <Button
                buttonType="success"
                clicked={this.props.continuePurchase}
                >CONTINUE</Button>
            </React.Fragment>
        )
    }

}

export default OrderSummary;