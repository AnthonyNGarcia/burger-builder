import * as actionTypes from '../actions/actionTypes';
import { addOrder } from '../mockBackend';

const initialState = {
    orders: [],
    loading: false,
    purchased: false
}

const purchaseInit = (state, action) => {
    return {
        ...state,
        purchased: false
    }
}

const fetchOrPurchaseStart = (state, action) => {
    return {
        ...state,
        loading: true
    }   
}

const purchaseBurgerSuccess = (state, action) => {
    const newOrder = {
        ...action.orderData,
        id: action.orderId,
        userId: action.userId,
    }
    addOrder(newOrder);
    return {
        ...state,
        loading: false,
        orders: state.orders.concat(newOrder),
        purchased: true
    }
}

const fetchOrPurchaseFailure = (state, action) => {
    return {
        ...state,
        loading: false
    }
}

const fetchOrdersSuccess = (state, action) => {
    return {
        ...state,
        orders: action.orders,
        loading: false
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.PURCHASE_INIT: return purchaseInit(state, action);
        case actionTypes.PURCHASE_BURGER_START:
        case actionTypes.FETCH_ORDERS_START: return fetchOrPurchaseStart(state, action);
        case actionTypes.PURCHASE_BURGER_SUCCESS: return purchaseBurgerSuccess(state, action);
        case actionTypes.PURCHASE_BURGER_FAILURE:
        case actionTypes.FETCH_ORDERS_FAILURE: return fetchOrPurchaseFailure(state, action);
        case actionTypes.FETCH_ORDERS_SUCCESS: return fetchOrdersSuccess(state, action);
        default: return state;
    }
};

export default reducer;