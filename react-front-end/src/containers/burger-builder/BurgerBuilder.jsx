import React, { Component } from 'react';
import Burger from '../../components/burger/Burger';
import BuildControls from '../../components/burger/build-controls/BuildControls';
import Modal from '../../components/ui/modal/Modal';
import OrderSummary from '../../components/burger/order-summary/OrderSummary';
import Spinner from '../../components/ui/spinner/Spinner';
import withErrorHandler from '../../components/hoc/with-error-handler/withErrorHandler';
import axios from '../../axios-orders';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

export class BurgerBuilder extends Component {

    async componentDidMount() {
        if (!this.props.building) {
            this.props.onInitIngredients();
        } else {
            this.props.onSetAuthRedirectPath('/');
        }
    }

    state = {
        purchasing: false
    }

    updatePurchaseState () {
        let newPurchaseState = false;
        for (let ingredient in this.props.ingredients) {
            if (this.props.ingredients[ingredient] > 0) {
                newPurchaseState = true;
                break;
            }
        }
        return newPurchaseState;
    }

    purchaseHandler = ()  => {
        if (this.props.token) {
            this.setState({purchasing: true});
        } else {
            this.props.onSetAuthRedirectPath('/checkout');
            this.props.history.push('/auth');
        }
    }

    cancelPurchaseHandler = () => {
        this.setState({purchasing: false});
    }

    continuePurchaseHandler = async () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {
        const disabledInfo = {
            ...this.props.ingredients
        }

        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let burger = this.props.error ? <p>The ingredients could not be loaded.</p> : (<Spinner />);
        let orderSummary = null;

        if (this.props.ingredients) {
            burger = (<React.Fragment>
            <Burger ingredients = {this.props.ingredients} />
            <BuildControls 
                addIngredient={(type) => this.props.addIngredient(type)}
                removeIngredient={(type) => this.props.removeIngredient(type)}
                disabled={disabledInfo}
                purchasable={this.updatePurchaseState()}
                price={this.props.totalPrice}
                updatePurchaseState={this.purchaseHandler}/>
            </React.Fragment>);

            orderSummary = (<OrderSummary
                ingredients={this.props.ingredients}
                cancelPurchase={this.cancelPurchaseHandler}
                continuePurchase={this.continuePurchaseHandler}
                price={this.props.totalPrice}
                />);
        }

        return (
            <React.Fragment>
                <Modal
                    show={this.state.purchasing}
                    closeModal={this.cancelPurchaseHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        token: state.auth.token,
        building: state.burgerBuilder.building
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addIngredient: (ingredientName) => dispatch(actions.addIngredient(ingredientName)),
        removeIngredient: (ingredientName) => dispatch(actions.removeIngredient(ingredientName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit()),
        onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));