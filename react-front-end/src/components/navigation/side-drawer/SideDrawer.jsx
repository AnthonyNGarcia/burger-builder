import React from 'react';
import Logo from '../../logo/Logo';
import NavigationItems from '../navigation-items/NavigationItems';
import Backdrop from '../../ui/backdrop/Backdrop';
import './SideDrawer.css'

const sideDrawer = (props) => {

    let attachedClasses = ['side-drawer', 'close'];

    if (props.open) {
        attachedClasses = ['side-drawer', 'open'];
    }

    return (
        <React.Fragment>
            <Backdrop show={props.open} escapeBackdrop={props.closeSideDrawer}/>
            <div className={attachedClasses.join(' ')} onClick={props.closeSideDrawer}>
                <button onClick={props.closeSideDrawer}
                style={{height: "11%",
                marginBottom: "32px"}}>
                    <Logo/>
                </button>
                <nav>
                    <NavigationItems/>
                </nav>
            </div>
        </React.Fragment>
    )
}

export default sideDrawer;