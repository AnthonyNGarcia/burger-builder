import React, { Component } from 'react';
import Backdrop from '../backdrop/Backdrop';
import './Modal.css';

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.show!==nextProps.show || this.props.children!==nextProps.children;
    }

    render() {
        return (
            <React.Fragment>
                <Backdrop
                show={this.props.show}
                escapeBackdrop={this.props.closeModal}/>
                <div
                className="modal"
                style={{
                    transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: this.props.show ? '1': '0'
                    }}>
                    {this.props.children}
                </div>
            </React.Fragment>
            
        )
    }
}

export default Modal;