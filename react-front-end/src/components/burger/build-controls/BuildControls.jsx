import React from 'react';
import { connect } from 'react-redux';
import BuildControl from './build-control/BuildControl'
import './BuildControls.css';

const controls = [
    { label: 'Lettuce', type: 'lettuce' },
    { label: 'Patty', type: 'patty' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Bacon', type: 'bacon' },
];

const buildControls = (props) => {
    return (
        <div className="build-controls">
            <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
            {controls.map(control => {
                return (<BuildControl
                    key={control.label}
                    label={control.label}
                    addIngredient={() => props.addIngredient(control.type)}
                    removeIngredient={() => props.removeIngredient(control.type)}
                    disabled={props.disabled[control.type]}
                ></BuildControl>)

            })}
            <button
                className="order-button"
                disabled={!props.purchasable}
                onClick={props.updatePurchaseState}>{props.token ? "Place Order" : "Sign Up To Order"}</button>

        </div>
    )
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(buildControls);