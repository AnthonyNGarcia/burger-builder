import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

export const addIngredient = (ingredientName) => {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingredientName: ingredientName
    }
}

export const removeIngredient = (ingredientName) => {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingredientName: ingredientName
    }
}

const setIngredients = (ingredients) => {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients: ingredients
    }
}

const fetchIngredientsFailed = () => {
    return {
        type: actionTypes.FETCH_INGREDIENTS_FAILED
    }
}

export const initIngredients = () => {
    return async dispatch => {
        await new Promise( _ => setTimeout(_, 500));
        const response = await axios.get('/posts');
        if (response && response.status === 200) {
            const mockDataIngredients = {
                lettuce: 0,
                bacon: 0,
                cheese: 0,
                patty: 0
            }
            dispatch(setIngredients(mockDataIngredients));
        } else {
            dispatch(fetchIngredientsFailed());
        }
    }
}