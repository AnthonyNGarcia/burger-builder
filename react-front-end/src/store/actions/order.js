import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';
import { getOrders, getNewKey } from '../mockBackend';

const purchaseBurgerSuccess = (id, userId, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData: orderData,
        userId: userId,
    }
}

const purchaseBurgerFail = (error) => {
    console.log(error);
    return {
        type: actionTypes.PURCHASE_BURGER_FAILURE,
        error: true
    }
}

const purchaseBurgerStart = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_START,
    }
}

export const purchaseBurger = (userId, orderData) => {
    return async dispatch => {
        dispatch(purchaseBurgerStart());
        await new Promise( _ => setTimeout(_, 1000));
        const response = await axios.post('/posts')
        if (response && response.status === 201) {
            dispatch(purchaseBurgerSuccess(getNewKey(), userId, orderData));
        } else {
            dispatch(purchaseBurgerFail("Network Error"));
        }
    }
}

export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_INIT,
    }
}

const fetchOrdersStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START,
    }
}

const fetchOrdersSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders: orders
    }
}

const fetchOrdersFailure = (error) => {
    return {
        type: actionTypes.FETCH_ORDERS_FAILURE,
        error: error
    }
}

export const fetchOrders = (userId) => {
    return async dispatch => {
        dispatch(fetchOrdersStart());
        await new Promise( _ => setTimeout(_, 500));
        const response = await axios.get('/posts');
        const fetchedOrders = getOrders(userId);
        if (response && response.status === 200) {
            dispatch(fetchOrdersSuccess(fetchedOrders));
        } else {
            dispatch(fetchOrdersFailure("Network Error"));
        }
    }
} 