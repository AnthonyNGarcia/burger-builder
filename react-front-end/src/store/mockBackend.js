const dummyDB = [
    {
        id: 1,
        userId: 6,
        ingredients: {
            lettuce: 3,
            patty: 2,
            bacon: 0,
            cheese: 0
        },
        price: 4.75
    },
    {
        id: 2,
        userId: 5,
        ingredients: {
            lettuce: 1,
            patty: 4,
            bacon: 0,
            cheese: 1
        },
        price: 11.50
    },
    {
        id: 3,
        userId: 6,
        ingredients: {
            lettuce: 1,
            patty: 2,
            bacon: 2,
            cheese: 2
        },
        price: 9.25
    },
];

const dummyAuth = [
    {
        id: 4,
        email: 'test@test.com',
        password: 'password'
    },
];

const dummyValidTokenStore = [];

let keyCounter = 4;

export const getNewKey = () => {
    keyCounter++;
    return keyCounter;
}

export const addOrder = (order) => {
    dummyDB.push(order);
}

export const getOrders = (userId) => {
    return dummyDB.filter(order => order.userId === userId);
}

export const destroyToken = (token) => {
    const indexToRemove = dummyValidTokenStore.findIndex(storeToken => storeToken === token);
    dummyValidTokenStore.splice(indexToRemove, 1);
}

const tokenGenerator = (tokenLength) => {
    if (tokenLength <= 5) {
        tokenLength = 5;
    }
    let token = '';
    for (let i = 0; i < tokenLength; i++) {
        let char = Math.random() * 100;
        if (char <= 10) {
            char = 'A';
        } else if (char <= 20) {
            char = 'z';
        } else if (char <= 30) {
            char = '2';
        } else if (char <= 40) {
            char = '7';
        } else if (char <= 50) {
            char = 'W';
        } else if (char <= 60) {
            char = 'b';
        } else if (char <= 70) {
            char = '5';
        } else if (char <= 80) {
            char = 'T';
        } else if (char <= 90) {
            char = 'e';
        } else {
            char = 'U';
        }
        token += char
    }

    if (dummyValidTokenStore.includes(token)) {
        return tokenGenerator(tokenLength);
    } else {
        dummyValidTokenStore.push(token);
        return token;
    }
}

export const addUser = (email, password) => {
    const existingUsers = dummyAuth.filter(user => user.email === email);
    if (existingUsers === null || existingUsers.length === 0) {
        const newUser = {
            id: getNewKey(),
            email: email,
            password: password
        };
        dummyAuth.push(newUser)
        return {
            userId: newUser.id,
            token: tokenGenerator(12)
        };
    } else {
        throw new Error('Oops! There is already a user by that email!');
    }
}

export const loginUser = (email, password) => {
    const existingUsers = dummyAuth.filter(user => user.email === email && user.password === password);
    if (existingUsers === null || existingUsers.length === 0) {
        return new Error('Invalid login credentials!');
    } else {
        const existingUser = existingUsers[0];
        return {
            userId: existingUser.id,
            token: tokenGenerator(12)
        };
    }
}