import React from 'react';
import Input from '../../components/ui/input/Input';
import Button from '../../components/ui/button/Button';
import { connect } from 'react-redux';
import Spinner from '../../components/ui/spinner/Spinner';
import * as actions from '../../store/actions/index';
import './Auth.css';
import { Redirect } from 'react-router';

class Auth extends React.Component {

    state = {
        authForm: [
            {
                elementName: 'email',
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your Email Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            {
                elementName: 'password',
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'New Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        ],
        isSigningUp: true,
    };

    componentDidMount() {
        if (!this.props.buildingBurger) {
            this.props.onSetAuthRedirectPath();
        }
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return { isSigningUp: !prevState.isSigningUp };
        })
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, elementName) => {
        const updatedAuthForm = [...this.state.authForm];
        const formElementIndex = updatedAuthForm.findIndex((formElement) => formElement.elementName === elementName);
        const updatedFormElement = { ...updatedAuthForm[formElementIndex] };
        updatedFormElement.value = event.target.value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedAuthForm[formElementIndex] = updatedFormElement;

        let formIsNowValid = true;

        for (let formElement of updatedAuthForm) {
            formIsNowValid = formIsNowValid && formElement.valid;
        }
        this.setState({ authForm: updatedAuthForm, formIsValid: formIsNowValid });
    }

    authHandler = async (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElement of this.state.authForm) {
            let newKey = formElement.elementName;
            let newValue = formElement.value;
            formData[newKey] = newValue;
        }
        let method = this.state.isSigningUp ? 'SIGN-UP' : 'SIGN-IN'
        this.props.onAuth(formData['email'], formData['password'], method);

    }


    render() {
        let form = (<form onSubmit={(event) => this.authHandler(event)}>
            {this.state.authForm.map(element => (
                <Input key={element.elementName}
                    elementName={element.elementName}
                    elementType={element.elementType}
                    elementConfig={element.elementConfig}
                    value={element.value}
                    changed={(event) => this.inputChangedHandler(event, element.elementName)}
                    invalid={!element.valid}
                    shouldValidate={element.validation}
                    touched={element.touched} />
            ))}
            <Button buttonType="success" disabled={!this.state.formIsValid}>{this.state.isSigningUp ? 'SIGN-UP' : 'SIGN-IN'}</Button>
        </form>);

        if (this.props.loading) {
            form = <Spinner />;
        }

        let errorMessage = null;

        if (this.props.error) {
            errorMessage = <p>{this.props.error}</p>
        }

        let redirect = null;

        if (this.props.token) {
            redirect = <Redirect to={this.props.authRedirectPath} />
        }
        return (
            <div className="auth">
                {redirect}
                {errorMessage}
                {form}
                <Button buttonType="danger" disabled={!this.state.formIsValid} clicked={this.switchAuthModeHandler}>SWITCH TO {this.state.isSigningUp ? 'SIGN-IN' : 'SIGN-UP'}</Button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        token: state.auth.token,
        buildingBurger: state.burgerBuilder.building,
        authRedirectPath: state.auth.authRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, method) => dispatch(actions.auth(email, password, method)),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/')),
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(Auth);