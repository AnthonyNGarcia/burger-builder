import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import CheckoutSummary from '../../components/order/checkout-summary/CheckoutSummary';
import ContactData from './contact-data/ContactData';
import { connect } from 'react-redux';

class Checkout extends Component {

    cancelCheckoutHandler = () => {
        this.props.history.goBack();
    }

    continueCheckoutHandler = () => {
        this.props.history.replace('/checkout/contact-data');
    }

    render() {
        let summary = <Redirect to='/' />
        if (this.props.ingredients) {
            const redirectIfPurchased = this.props.purchased ? <Redirect to="/" /> : null;
            summary = (<React.Fragment>
                {redirectIfPurchased}
                <CheckoutSummary
                    ingredients={this.props.ingredients}
                    cancelCheckout={this.cancelCheckoutHandler}
                    continueCheckout={this.continueCheckoutHandler}
                />
                <Route path={this.props.match.url + '/contact-data'} component={ContactData} />
            </React.Fragment>);
        }
        return summary;
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        purchased: state.orders.purchased,
    }
}

export default connect(mapStateToProps)(Checkout);