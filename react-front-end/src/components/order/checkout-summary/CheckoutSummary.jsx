import React from 'react';
import Burger from '../../burger/Burger';
import Button from '../../ui/button/Button';
import './CheckoutSummary.css';

const checkoutSummary = (props) => {

    return (
        <div className='checkout-summary'>
            <h1>We hope it tastes great!</h1>
            <div style={{width: '100%', margin: 'auto'}}>
                <Burger ingredients = {props.ingredients} />
            </div>
            <Button buttonType="danger"
            clicked={props.cancelCheckout}>CANCEL</Button>
            <Button buttonType="success"
            clicked={props.continueCheckout}>CONTINUE</Button>
        </div>
    )
}

export default checkoutSummary;