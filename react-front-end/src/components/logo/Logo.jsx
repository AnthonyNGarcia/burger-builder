import React from 'react';
import './Logo.css'
import burgerLogo from '../../assets/images/burger-logo.png';

const logo = (props) => (
    <div className="logo"
        style={props.inlineStyles}>
        <img src={burgerLogo} alt="Burger Builder Logo"/>
    </div>
)

export default logo;