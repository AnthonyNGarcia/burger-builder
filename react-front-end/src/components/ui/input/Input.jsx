import React from 'react';
import './Input.css';

const input = (props) => {

    let inputElement = null;
    let inputClasses = ['input-element'];

    let validationError = null;

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push('invalid');
        validationError = (<p className='validation-error'>Please enter a valid {props.elementName}</p>);
    }

    switch (props.elementType) {
        case ('input'):
            inputElement = <input className={inputClasses.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed}/>
            break;
        case ('textarea'):
            inputElement = <textarea className={inputClasses.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed}/>
            break;
        case ('select'):
            inputElement = (
            <select className={inputClasses.join(' ')} value={props.value} onChange={props.changed}>
                {props.elementConfig.options.map( option => {
                    return (<option key={option.value} value={option.value}>
                        {option.displayValue}
                    </option>)
                })}
            </select>)
            break;
        default:
            inputElement = <input className={inputClasses.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed}/>
    }

    return (
        <div className="input">
            <label className="label">{props.label}</label>
            {inputElement}
            {validationError}
        </div>
    )
}

export default input;