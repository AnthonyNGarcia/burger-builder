import React from 'react';
import './Burger.css';
import BurgerIngredient from './burger-ingredient/BurgerIngredient';

const burger = (props) => {

    // const transformedIngredients = Object.keys(props.ingredients)
    //     .map(ingredientKey => {
    //         return  [...Array(props.ingredients[ingredientKey])]
    //             .map( (_, index) => {
    //                 return <BurgerIngredient key = {ingredientKey + index} type = {ingredientKey} />
    //             });
    //     });
    
    const transformedIngredients = [];
    for (let ingredient in props.ingredients) {
        let ingredientCount = props.ingredients[ingredient];
        for (let i  = 0; i < ingredientCount; i++) {
            // console.log(ingredient + i);
            transformedIngredients.push(
                (<div key={ingredient + i} className={ingredient} ></div>)
            )
        }
    }

    //console.log(transformedIngredients);

    if (transformedIngredients.length === 0) {
        transformedIngredients.push(<p key="default-key">Please start adding ingredients!</p>);
    }

    return (
        <div className="burger">
            <BurgerIngredient type = "bread-top" />
            {transformedIngredients}
            <BurgerIngredient type = "bread-bottom" />
        </div>
    );
}

export default burger;