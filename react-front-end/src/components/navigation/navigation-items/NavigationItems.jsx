import React from 'react';
import NavigationItem from './navigation-item/NavigationItem';
import { connect } from 'react-redux';
import './NavigationItems.css';

export const NavigationItems = (props) => (
    <ul className="navigation-items">
        <NavigationItem exact link="/">Burger Builder</NavigationItem>
        {props.isAuth ? <NavigationItem link="/orders">Orders</NavigationItem> : null}
        {props.isAuth
            ? <NavigationItem link="/logout">Logout</NavigationItem>
            : <NavigationItem link="/auth">Authenticate</NavigationItem>
        }
    </ul>
)

const mapStateToProps = state => {
    return {
        isAuth: state.auth.token !== null
    }
}

export default connect(mapStateToProps)(NavigationItems);