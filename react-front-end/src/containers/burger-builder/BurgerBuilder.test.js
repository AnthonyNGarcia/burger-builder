import { BurgerBuilder } from './BurgerBuilder';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import BuildControls from '../../components/burger/build-controls/BuildControls';

configure({adapter: new Adapter()});

describe('Testing the redux-stripped <BurgerBuilder />', () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<BurgerBuilder onInitIngredients={() => {} } onSetAuthRedirectPath={()=>{}}/>);
    });

    it('should render <BuildControls /> when receiving ingredients', () => {
        wrapper.setProps({ingredients: {lettuce: 0}});
        expect(wrapper.find(BuildControls)).toHaveLength(1);
    });
})