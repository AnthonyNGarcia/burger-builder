import React, { Component } from 'react';
import Button from '../../../components/ui/button/Button';
import Spinner from '../../../components/ui/spinner/Spinner';
import Input from '../../../components/ui/input/Input';
import axios from '../../../axios-orders';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import withErrorHandler from '../../../components/hoc/with-error-handler/withErrorHandler';
import * as actionTypes from '../../../store/actions/index';
import './ContactData.css';

class ContactData extends Component {
    state ={
        orderForm: [
            {
                elementName: 'name',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            {
                elementName: 'street',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Street Address'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            {
                elementName: 'zipcode',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Postal Code (Zip Code)'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 5
                },
                valid: false,
                touched: false
            },
            {
                elementName: 'country',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Country'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            {
                elementName: 'email',
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Email Address'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            {
                elementName: 'delivery-method',
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: 'fastest', displayValue: 'Fastest'},
                        {value: 'standard', displayValue: 'Standard'},
                        {value: 'economy', displayValue: 'Economy'}
                    ]
                },
                value: 'fastest',
                valid: true
            },
        ],
        formIsValid: false,
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }
        
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, elementName) => {
        const updatedOrderForm = [...this.state.orderForm];
        const formElementIndex = updatedOrderForm.findIndex( (formElement) => formElement.elementName===elementName);
        const updatedFormElement = {...updatedOrderForm[formElementIndex]};
        updatedFormElement.value = event.target.value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedOrderForm[formElementIndex] = updatedFormElement;

        let formIsNowValid = true;

        for (let formElement of updatedOrderForm) {
            formIsNowValid = formIsNowValid && formElement.valid;
        }
        this.setState({orderForm: updatedOrderForm, formIsValid: formIsNowValid});
    }

    orderHandler = async (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElement of this.state.orderForm ) {
            let newKey = formElement.elementName;
            let newValue = formElement.value;
            formData[newKey] = newValue;
        }
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
            orderData: formData,
        }
        this.props.purchaseBurger(this.props.userId, order);

    }

    render () {

        let form = (<form onSubmit={this.orderHandler}>
            {this.state.orderForm.map(element => (
                <Input key={element.elementName}
                elementName={element.elementName}
                elementType={element.elementType}
                elementConfig={element.elementConfig}
                value={element.value}
                changed={(event) => this.inputChangedHandler(event, element.elementName)}
                invalid={!element.valid}
                shouldValidate={element.validation}
                touched={element.touched}/>
            ))}
            <Button buttonType="success" disabled={!this.state.formIsValid}>ORDER</Button>
        </form>);

        if (this.props.loading) {
            form = (<Spinner />);
        }
        return (
            <div className="contact-data">
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        loading: state.orders.loading,
        userId: state.auth.userId,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        purchaseBurger: (userId, orderData) => dispatch(actionTypes.purchaseBurger(userId, orderData)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withErrorHandler(ContactData, axios)));