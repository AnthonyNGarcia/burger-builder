import React from 'react';
import Logo from '../../logo/Logo';
import NavigationItems from '../navigation-items/NavigationItems';
import DrawerToggle from '../side-drawer/drawer-toggle/DrawerToggle';
import './Toolbar.css';

const toolbar = (props) => (
    <header className="toolbar">
        <DrawerToggle clicked={props.showSideDrawer}/>
        <Logo inlineStyles={{
            height: "80%",
        }}/>
        <nav className="desktop-only">
            <NavigationItems/>
        </nav>
    </header>
);

export default toolbar;