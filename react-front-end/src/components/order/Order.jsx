import React from 'react';
import './Order.css';


const order = (props) => {

    const transformedIngredients = [];

    for (let ingredientName in props.ingredients) {
        transformedIngredients.push({name: ingredientName, amount: props.ingredients[ingredientName]});
    }

    const output = transformedIngredients.map(ingredient => (
        <span style={{textTransform: 'capitalize',
        margin: '0 8px',
        display: 'inline-block',
        border: '1px solid #ccc',
        padding: '5px'}} key={ingredient.name}>{ingredient.name + ' (' + ingredient.amount + ')'}</span>
    ));

    return (
        <div className="order">
            <p>Ingredients: {output}</p>
            <p>Price: <strong>{props.price}</strong></p>
        </div>
    );
}

export default order;