import * as actionTypes from './actionTypes';
import { addUser, loginUser, destroyToken } from '../mockBackend';

const EXPIRATION_TIME = 3600*1000;

const authStart = () => {
    return {
        type: actionTypes.AUTH_START,
    }
};

const authSuccess = (responseData) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: responseData.token,
        userId: responseData.userId
    }
};

const authFailure = (errorMessage) => {
    return {
        type: actionTypes.AUTH_FAILURE,
        errorMessage: errorMessage
    }
};

export const logout = (token) => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationTime');
    localStorage.removeItem('userId');
    destroyToken(token);
    return {
        type: actionTypes.AUTH_LOGOUT,
        token: token
    }
}

const checkAuthTimeout = (expirationTime, token) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout(token));
        }, expirationTime);
    }
}

export const auth = (email, password, method) => {
    return async (dispatch) => {
        dispatch(authStart());
        let dbMethod = addUser;
        if (method === 'SIGN-IN') {
            dbMethod = loginUser;
        } else {
        }
        try {
            await new Promise(_ => setTimeout(_, 300));
            const responseData = dbMethod(email, password);
            const expirationDate = new Date(new Date().getTime() + EXPIRATION_TIME);
            localStorage.setItem('token', responseData.token);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', responseData.userId);
            dispatch(authSuccess(responseData));
            dispatch(checkAuthTimeout(EXPIRATION_TIME, responseData.token));
        } catch (error) {
            dispatch(authFailure(error.message));
        }
    }
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate < new Date()) {
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess({token: token, userId: userId}));
                dispatch(checkAuthTimeout(expirationDate.getTime() - new Date().getTime()));
            }
        }
    }
}