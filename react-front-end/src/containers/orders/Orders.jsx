import React from 'react';
import Order from '../../components/order/Order';
import Spinner from '../../components/ui/spinner/Spinner';
import withError from '../../components/hoc/with-error-handler/withErrorHandler';
import axios from '../../axios-orders';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import { getNewKey } from '../../store/mockBackend';

class Orders extends React.Component {
    async componentDidMount() {
        this.props.fetchOrders(this.props.userId);
    }

    render() {

        let orders = (<Spinner />);

        if (!this.props.token || this.props.error) {
            orders = <p>You must be signed in to access this feature!</p>
        } else if (!this.props.loading) {
            orders = this.props.orders.map(order => (
                <Order key={order.id + getNewKey()} ingredients={order.ingredients} price={order.price} />
            ));
        }

        return (
            <div>
                {orders}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        orders: state.orders.orders,
        loading: state.orders.loading,
        token: state.auth.token,
        userId: state.auth.userId,
        error: state.auth.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchOrders: (userId) => dispatch(actions.fetchOrders(userId)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withError(Orders, axios));